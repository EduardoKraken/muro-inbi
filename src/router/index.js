import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Posts from '../views/catalogos/Posts.vue'
import Img from '../views/catalogos/Img.vue'
import AprobarImagen from '../views/catalogos/AprobarImagen.vue'

import store from '@/store'
Vue.use(VueRouter)

const routes = [{
        path: '/:roll/:id/:group/:unidad',
        name: 'Post',
        component: Posts
    },
    {
        path: '/posts/:roll/:id/:group/:unidad',
        name: 'Posts',
        component: Posts
    },
    {
        path: '/img/:roll/:id/:group/:unidad',
        name: 'Img',
        component: Img
    },
    {
        path: '/',
        name: 'Home',
        component: Home
    },
]

const router = new VueRouter({
    routes
})

export default router